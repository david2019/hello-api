# Create an API #

Please read the information below and follow the requested guidelines.

### This repository is a starting (and ending) point for a coding exercise for potential Blick developers ###

* A branch of this repository should exist with your initials on it. Please work from that branch.

### Basic Instructions ###

The idea of the exercise is to create an API which returns a greeting. For the purposes of this exercise the 
greeting can be returned as plain text or JSON. As described below a GET from the endpoint will return 
"Hello, Guest" or "Hello, <name>" if a name is stored; a POST will store the name; and a DELETE will remove 
the name. 

The endpoint URL is up to you, except that it should end with /greeting/.

While there will be no official tracking of time spent, it is the intention of this exercise that it take **no more than 2-3 hours.**

#### Specifics: ####

* Create a fork from the branch with your initials and use it as a starting point
* Create a REST API using either flask or django
* Implement three REST verbs:
    - GET - returns a greeting using the currently stored name, or Guest if no name is stored. 
    - POST - sets/stores the name to use persistently on the system
        * The payload containing the name should be a JSON object.
    - DELETE - removes the current name
* Update the readme file with any instructions to setup and run the project and any comments you might have [Markdown help](https://bitbucket.org/tutorials/markdowndemo)
* Create a pull request for the master branch from your branch to check in your code

### Acceptance Criteria ###

* The application needs to do only the three things specified above
* No bonuses will be awarded for extra features/functionality
* It may run locally using an unsecure dev server (but it needs to run)
* The **name must persist between runs until deleted**
* Any form of local storage may be used for that persistence

### What happens next? ###

* During your onsite interview team members may ask you to discuss your choices and assumptions in implementing this project.
* Be prepared to also talk about any possible issues or enhancements you might make on your design. 

### Things to think about for discussion ###

**** Testing:
*verify django-tastypie is install
*python -3.6 and django 2.2
*After loading the project, 
	*cd hello
	*python manage.py runserver
*use postman app and point to http://127.0.0.1:8000/api/person
     *Get
	 *Post using json format as {"name":"Coco Mist"}
	 *delete http://127.0.0.1:8000/api/person/<id>
*Using admin:  http://127.0.0.1:8000/admin   (logon: admin/123456)
	add peron

* Coding style
Simple and make it work

* Future proofing the design
Add more detail about the name
explore other package, like djangorestframe to make it work better.
Add error handling to deal with different scenarios.

